class PostDataService {

    /*@ngInject*/
    constructor($http, $q){
        this.$http = $http;
        this.$q = $q;
    }

    getData() {
        return this.$http.get('./app/assets/posts.json')
            .then(res => res.data,
                    err => this.$q.reject(err.error));
    }
}

export default angular.module('Service', [])
    .service('PostDataService', PostDataService)
    .name;




