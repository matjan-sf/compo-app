import angular from "angular";
import "./comment-form.scss";
import template from "./comment-form.html";
import uiRouter from "angular-ui-router";
import UserService from "../../../service/user.service"

class CommentFormController {
    /*@ngInject*/
    constructor(UserService) {
        const vm = this;
        vm.author = angular.copy(UserService.currentUser);
        vm.commentInputForm = '';
        vm.commentInputFormData =  {
            postId: null,
            text:'',
            createDate: null
        };

        vm.addComment = () => {
            if (vm.commentInputForm.$valid) {
                this.addCommentForm({danee: angular.copy(vm.commentInputFormData)});
                vm.commentInputFormData.text = null;
            }
        };
    }
}

export default angular.module('commentForm', [uiRouter, UserService])
    .component('commentForm', {
        template,
        controller: CommentFormController,
        controllerAs: 'vm',
        bindings: {
            addCommentForm: '&'
        }
    })
    .name


