import angular from "angular";
import "./comments.scss";
import template from "./comments.html";
import uiRouter from "angular-ui-router";
import * as _ from 'lodash';

class CommentsController {
    showComments = false;
    showLastComment = false;

    /*@ngInject*/
    constructor($scope) {
        const vm = this;

        // 'watch' do nasluchiwania zmian - onChanges nie reaguje na update bindingsow - blad angulara?
        $scope.$watch('vm.commentsData', comments => {
            if (_.isArray(comments) && comments.length){
                this.showLastComment = true;
            } else {
                this.showLastComment = false;
            }
        }, true);
    }

    onShowCommentsList() {
        this.showComments = true;
        this.showLastComment = false;
    }

    onHideCommentList(){
        this.showComments = false;
        this.showLastComment = true;
    }

    addNewComment(commentsData){
        this.addCommentData({dataKolejna: angular.copy(commentsData)});
    }
}

export default angular.module('comments', [uiRouter])
    .component('comments', {
        template,
        controller: CommentsController,
        controllerAs: 'vm',
        bindings: {
            commentsData: '=',
            addCommentData: '&'
        }
    })
    .name


