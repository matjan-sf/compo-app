import angular from "angular";
import "./comment-list.scss";
import template from "./comment-list.html";
import uiRouter from "angular-ui-router";

class CommentListController {
    /*@ngInject*/
    constructor() {
    }

}

export default angular.module('commentList', [uiRouter])
    .component('commentList', {
        template,
        controller: CommentListController,
        controllerAs: 'vm',
        bindings: {
            commentsList: '<'
        }
    })
    .name


