import angular from "angular";
import "./comment-author-avatar.scss";
import template from "./comment-author-avatar.html";
import uiRouter from "angular-ui-router";

class CommentAuthorAvatarController {
    /*@ngInject*/
    constructor() {
    }
}

export default angular.module('commentAuthorAvatar', [uiRouter])
    .component('commentAuthorAvatar', {
        template,
        controller: CommentAuthorAvatarController,
        controllerAs: 'vm',
        bindings: {
            commentAvatar: '<'
        }
    })
    .name


