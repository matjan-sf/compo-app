import angular from "angular";
import "./comment-author-text.scss";
import template from "./comment-author-text.html";
import uiRouter from "angular-ui-router";

class CommentAuthorTextController {
    /*@ngInject*/
    constructor() {
    }
}

export default angular.module('commentAuthorText', [uiRouter])
    .component('commentAuthorText', {
        template,
        controller: CommentAuthorTextController,
        controllerAs: 'vm',
        bindings: {
            commentText: '<'
        }
    })
    .name


