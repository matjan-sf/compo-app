import angular from "angular";
import "./comment.scss";
import template from "./comment.html";
import uiRouter from "angular-ui-router";

class CommentController {
    /*@ngInject*/
    constructor() {
    }
}

export default angular.module('comment', [uiRouter])
    .component('comment', {
        template,
        controller: CommentController,
        controllerAs: 'vm',
        bindings: {
            commentDetails: '<'
        }
    })
    .name


