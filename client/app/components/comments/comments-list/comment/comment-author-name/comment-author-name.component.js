import angular from "angular";
import "./comment-author-name.scss";
import template from "./comment-author-name.html";
import uiRouter from "angular-ui-router";

class CommentAuthorNameController {
    /*@ngInject*/
    constructor() {
    }
}

export default angular.module('commentAuthorName', [uiRouter])
    .component('commentAuthorName', {
        template,
        controller: CommentAuthorNameController,
        controllerAs: 'vm',
        bindings: {
            commentName: '<'
        }
    })
    .name


