import angular from "angular";
import "./comment-last.scss";
import template from "./comment-last.html";
import uiRouter from "angular-ui-router";

class CommentLastController {
    /*@ngInject*/
    constructor() {
    }
}

export default angular.module('commentLast', [uiRouter])
    .component('commentLast', {
        template,
        controller: CommentLastController,
        controllerAs: 'vm',
        bindings: {
            commentLast: '<'
        }
    })
    .name


