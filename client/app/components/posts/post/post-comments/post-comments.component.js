import angular from "angular";
import "./post-comments.scss";
import template from "./post-comments.html";
import uiRouter from "angular-ui-router";

class PostCommentsController {
    /*@ngInject*/
    constructor() {
        const vm = this;
        vm.commentsLocal = null;
    }

    passCommentData(comments) {
        this.nextCommentDataPass({dataNew: comments})
    }
}



export default angular.module('postComments', [uiRouter])
    .component('postComments', {
        template,
        controller: PostCommentsController,
        controllerAs: 'vm',
        bindings: {
            comments:'=',
            nextCommentDataPass: '&'
        }
    })
    .name


