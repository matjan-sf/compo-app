import angular from 'angular';
import uiRouter from 'angular-ui-router';
import template from './post-date.html'
import './post-date.scss'


class PostDateController{
    /*@ngInject*/
    constructor() {
        let vm = this;

    }
}

export default angular.module('postDate', [uiRouter])
    .component('postDate', {
        controller: PostDateController,
        controllerAs: 'vm',
        template,
        bindings: {
            postDate: '<'
        }
    }).name;
