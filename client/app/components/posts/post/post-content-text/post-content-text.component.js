import angular from 'angular';
import uiRouter from 'angular-ui-router';
import template from './post-content-text.html'
import './post-content-text.scss'


class PostContentTextController{
    /*@ngInject*/
    constructor(){
        let vm = this;
    }
}

export default angular.module('postContentText', [uiRouter])
    .component('postContentText', {
        controller: PostContentTextController,
        controllerAs: 'vm',
        template,
        bindings: {
            postText: '<'
        }
    }).name;
