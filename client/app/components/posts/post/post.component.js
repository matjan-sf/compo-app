import angular from 'angular';
import uiRouter from 'angular-ui-router';
import template from './post.html'
import './post.scss'


class PostController{
    /*@ngInject*/
    constructor($scope){
        let vm = this;
        vm.postLocal = null;
        vm.posts = null;
        vm.clicks= false;
        $scope.$watch('vm.post', post => {
            this.postLocal = post
        }, true);

    }
    $onChanges() {

    }

    likeClick(){
        if (!this.clicks){
            this.clicks = true;
            this.postLocal.likes++;
        } else {
            this.clicks = false;
            this.postLocal.likes--;

        }
        // console.log(this.clicks);
        // console.log(this.postLocal.likes)
    }



    getNewComment(post){
        post.postId = angular.copy(this.post.id);
        this.newCommentData({dataComment: angular.copy(post)})
    }


}
export default angular.module('post', [uiRouter])
    .component('post', {
        controller: PostController,
        controllerAs: 'vm',
        template,
        bindings: {
            post: '=',
            newCommentData: '&'
        }
    }).name;
