import angular from 'angular';
import template from './post-author-avatar.html';
import './post-author-avatar.scss'


class PostAuthorAvatarController{
    constructor(){
        const vm = this;

    }
}

export default angular.module('postAuthorAvatar', [])
    .component('postAuthorAvatar', {
        controller: PostAuthorAvatarController,
        controllerAs: 'vm',
        template,
        bindings: {
            postAvatar: '<'
        }
    })
    .name;
