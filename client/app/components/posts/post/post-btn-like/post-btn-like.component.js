import angular from 'angular';
import uiRouter from 'angular-ui-router';
import template from './post-btn-like.html'
import './post-btn-like.scss'


class PostBtnLikeController{
    constructor(){

    }
}

export default angular.module('postBtnLike', [uiRouter])
    .component('postBtnLike', {
        controller: PostBtnLikeController,
        controllerAs: 'vm',
        template,
        bindings: {}
    }).name;
