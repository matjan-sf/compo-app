import angular from "angular"
import uiRouter from 'angular-ui-router'
import template from './post-menu-toggle.html'
import './post-menu-toggle.scss'


class PostMenuToggleController {
    constructor()
    {}}

export default  angular.module('postMenuToggle', [uiRouter])
    .component('postMenuToggle', {
        bindings: '<',
        template,
        controller: PostMenuToggleController,
        controllerAs: 'vm'
    })
        .name
