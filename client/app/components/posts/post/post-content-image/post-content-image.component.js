import angular from 'angular';
import uiRouter from 'angular-ui-router';
import template from './post-content-image.html'
import './post-content-image.scss'


class PostContentImageController{
    constructor(){

    }
}

export default angular.module('postContentImage', [uiRouter])
    .component('postContentImage', {
        controller: PostContentImageController,
        controllerAs: 'vm',
        template,
        bindings: {
            postImage: '<'
        }
    }).name;
