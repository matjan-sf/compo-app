import angular from 'angular';
import uiRouter from 'angular-ui-router';
import template from './post-author-name.html'
import './post-author-name.scss'


class PostAuthorNameController{
    /*@ngInject*/
    constructor(){
        let vm = this;


    }
}

export default angular.module('postAuthorName', [uiRouter])
    .component('postAuthorName', {
        controller: PostAuthorNameController,
        controllerAs: 'vm',
        template,
        bindings: {
            postName: '<'
        }
    })
    .name;

