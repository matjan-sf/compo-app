import angular from 'angular';
import uiRouter from 'angular-ui-router';
import template from './post-btn-comment.html'
import './post-btn-comment.scss'


class PostBtnCommentController{
    constructor(){

    }
}

export default angular.module('postBtnComment', [uiRouter])
    .component('postBtnComment', {
        controller: PostBtnCommentController,
        controllerAs: 'vm',
        template,
        bindings: {}
    }).name;
