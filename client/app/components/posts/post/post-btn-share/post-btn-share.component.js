import angular from 'angular';
import uiRouter from 'angular-ui-router';
import template from './post-btn-share.html'
import './post-btn-share.scss'


class PostBtnShareController{
    constructor(){

    }
}

export default angular.module('postBtnShare', [uiRouter])
    .component('postBtnShare', {
        controller: PostBtnShareController,
        controllerAs: 'vm',
        template,
        bindings: {}
    }).name;
