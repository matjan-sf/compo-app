import angular from "angular";
import "./posts.scss";
import template from "./posts.html";
import uiRouter from "angular-ui-router";
import PostDataService from "../../service/data.service";
import UserService from "../../service/user.service";

class PostsController {
    /*@ngInject*/
    constructor(PostDataService, UserService) {
        const vm = this;
        vm.posts = [];
        vm.post = {};
        // vm.actionsLength= {
        //     likes: null,
        //     comments: null,
        //     shares: null
        // };
        vm.author = angular.copy(UserService.currentUser);
        // console.log(UserService);
        PostDataService.getData().then((data) => {
            vm.posts = data;
            vm.posts = _.map(vm.posts, postItem => {
                if (postItem) {
                    // this.actionsLength.likes = postItem.likes;
                    // this.actionsLength.comments = postItem.comments.length;
                    // this.actionsLength.shares = postItem.shares;
                    //
                    // // console.log(this.actionsLength.likes);
                    // // console.log(this.actionsLength.comments);
                    // // console.log(this.actionsLength.shares);

                    if (postItem.createDate) {
                        postItem.createDate = new Date(postItem.createDate);
                        postItem.comments = _.map(postItem.comments, commentDate => {
                            if (commentDate.createDate) {
                                commentDate.createDate = new Date(commentDate.createDate);
                            }
                            return commentDate
                        })
                    }
                }
                return postItem;
            });
        //sortowanie
        });
    }

    addNewComment(datta) {
        this.posts = _.map(this.posts, postItem => {
            if (postItem && postItem.id === datta.postId) {

                datta.createDate = new Date();
                datta.author = this.author;
                //autor - z serwisu - angularcopy - aby uniknac refernecji (nadpisywania sie obiektow)
                postItem.comments.push(datta);
            }
            return postItem;

        });
        // console.log(this.posts);
        // console.log(this.author)
    }

    $onChanges() {
        // console.log(data.postId);
    }
}


export default angular.module('posts', [uiRouter, UserService, PostDataService])
    .component('posts', {
        template,
        controller: PostsController,
        controllerAs: 'vm',
        bindings: {}
    })
    .name


