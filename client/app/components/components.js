import angular from 'angular';
import sideComponent from '../components/side-component/side-component.component'
import postAuthorAvatar from "./posts/post/post-author-avatar/post-author-avatar.component"
import postAuthorName from "./posts/post/post-author-name/post-author-name.component"
import postBtnLike from "./posts/post/post-btn-like/post-btn-like.component"
import postBtnShare from "./posts/post/post-btn-share/post-btn-share.component"
import post from "./posts/post/post.component"
import posts from "./posts/posts.component"
import postContentImage from "./posts/post/post-content-image/post-content-image.component"
import postContentText from "./posts/post/post-content-text/post-content-text.component"
import postBtnComment from "./posts/post/post-btn-comment/post-btn-comment.component"
import postMenuToggle from './posts/post/post-menu-toggle/post-menu-toggle.component'
import comments from "./comments/comments.component"
import commentAuthorName from "./comments/comments-list/comment/comment-author-name/comment-author-name.component"
import commentAuthorAvatar from "./comments/comments-list/comment/comment-author-avatar/comment-author-avatar.component"
import commentAuthorText from "./comments/comments-list/comment/comment-author-text/comment-author-text.component"
import commentsForm from "./comments/comment-form/comment-form.component"
import commentsLast from "./comments/comment-last/comment-last.component"
import commentList from "./comments/comments-list/comment-list.component"
import comment from "./comments/comments-list/comment/comment.component"
import postComments from "./posts/post/post-comments/post-comments.component"
import postDate from "./posts/post/post-date/post-date.component"








let componentModule = angular.module('app.components', [
    sideComponent,
    postAuthorAvatar,
    postAuthorName,
    postBtnLike,
    postBtnShare,
    post,
    posts,
    postContentImage,
    postContentText,
    postBtnComment,
    postMenuToggle,
    comments,
    commentsForm,
    commentsLast,
    commentList,
    comment,
    commentAuthorText,
    commentAuthorAvatar,
    commentAuthorName,
    postComments,
    postDate


])

.name;

export default componentModule;
