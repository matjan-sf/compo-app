import angular from 'angular';
import uiRouter from 'angular-ui-router';
import template from './side-component.html'
import './side-component.scss'


class SideComponentController{
    /*@ngInject*/
    constructor(){}}

export default angular.module('sideComponent', [uiRouter])
    .component('sideComponent', {
        controller: SideComponentController,
        controllerAs: 'vm',
        template,
        bindings: {}
    }).name;
