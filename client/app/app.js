import angular from 'angular';
import uiRouter from 'angular-ui-router';
import Common from './common/common';
import Components from './components/components';
import AppComponent from './app.component';
import 'normalize.css';
import Service from './service/data.service.js';
import UserService from './service/user.service';
import Shared from './shared/shared'

angular.module('app', [
    uiRouter,
    Common,
    Components,
    Shared,
    UserService,
    Service
  ])

  .config(($stateProvider) => {
    "ngInject";
    $stateProvider
      .state('posts-view', {
        url: '/post-view',
        component: 'postView'
      })
      .state('home', {
        url: '/home',
        component: 'home'
      })
  })





  // .config(($locationProvider) => {
  //   "ngInject";
  //   // @see: https://github.com/angular-ui/ui-router/wiki/Frequently-Asked-Questions
  //   // #how-to-configure-your-server-to-work-with-html5mode
  //   $locationProvider.html5Mode(true).hashPrefix('!');
  // })

  .component('app', AppComponent);
