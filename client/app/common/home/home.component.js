import angular from 'angular';
import uiRouter from 'angular-ui-router';
import template from './home.html'
import './home.scss'

class HomeController {
    constructor() {

    }
}


export default angular.module('home', [uiRouter])
    .component('home', {
        template,
        controller: HomeController,
        bindings: {},
    })

    .name;
