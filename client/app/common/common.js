import angular from 'angular';
import navbar from './navbar/navbar.component';
import user from './user/user';
import footer from './footer/footer.component';
import home from './home/home.component';
import postsView from '../common/posts-view/posts-view.component';


let commonModule = angular.module('app.common', [
    navbar,
    user,
    footer,
    home,
    postsView

])

.name;

export default commonModule;
