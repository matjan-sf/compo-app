import angular from 'angular';
import './footer.scss';
import uiRouter from 'angular-ui-router';
import template from './footer.html'

class FooterController {
  constructor() {
    this.name = 'footer';
    this.footerAnnoation = 'CompoApp - share your components!!'
  }
}


export default angular.module('footer', [uiRouter])
  .component('footer', {
    template,
    controller: FooterController,
    bindings: {},

  })

  .name;
