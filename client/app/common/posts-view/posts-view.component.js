import angular from "angular";
import "./posts-view.scss";
import template from "./posts-view.html";
import uiRouter from "angular-ui-router";

class PostsViewController {
    /*@ngInject*/
    constructor() {

}
}


export default angular.module('postView', [uiRouter])
    .component('postView', {
        template,
        controller: PostsViewController,
        controllerAs: 'vm',
        bindings: {}
    })
    .name


