import angular from 'angular';
import './navbar.scss';
import template from './navbar.html'
import uiRouter from 'angular-ui-router';
import UserService from "../../service/user.service";


class NavbarController {
    /*@ngInject*/
    constructor(UserService) {
        const vm = this;
        vm.name = 'navbar';
        vm.loggedUser = angular.copy(UserService.currentUser);
        // console.log(vm.loggedUser)
    }
}


export default angular.module('navbar', [uiRouter, UserService])
    .component('navbar', {
        template,
        controller: NavbarController,
        controllerAs: 'vm',
        bindings: {},
    })
    .name;
